# Getting Started

### Configure the gcloud credential helper
```
gcloud auth print-access-token
```

### Build and push container image to a container registry in Google Artifact Registry
```
gradlew.bat jib --image=asia-east2-docker.pkg.dev/glassy-archway-354310/docker-repo/example.com/demo
```