package com.example.demo;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class DemoController {

    private final String datasourceUsername;
    private final String datasourcePassword;

    public DemoController(
            @Value("${spring.datasource.username:not specified}") String datasourceUsername,
            @Value("${spring.datasource.password:not specified}") String datasourcePassword
    ) {
        this.datasourceUsername = datasourceUsername;
        this.datasourcePassword = datasourcePassword;
    }

    @GetMapping
    public String home() {
        log.info("Home");
        return "Welcome to home (username is " + datasourceUsername + " and password is " + datasourcePassword + ")";
    }
}
